# NetifyOS Security Policies

## Description

NetifyOS base operating system security policies.

## Version Branches

The master branch is not used in the NetifyOS project.  Instead, the version branches track the current stable development:

* netifyos7
* netifyos8
* and beyond

Name:     netifyos-base-security
Version:  7.5.0
Release:  1%{dist}
Summary:  NetifyOS system base security driver
License:  GPLv3 or later
URL:      https://www.netify.ai/resources/netifyos
Vendor:   Netify by eGloo
Packager: Netify by eGloo
Group:    Applications/System
Source:   %{name}-%{version}.tar.gz
Requires: netifyos-release >= 7
Requires: selinux-policy-targeted
Requires: util-linux
Provides: system-base-security

%description
NetifyOS system base security driver.

%prep
%setup -q
%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p -m 755 $RPM_BUILD_ROOT/etc/logrotate.d
mkdir -p -m 755 $RPM_BUILD_ROOT/etc/security/limits.d

install -m 755 95-netifyos.conf $RPM_BUILD_ROOT/etc/security/limits.d/

%post
logger -p local6.notice -t installer "netifyos-base-security - installing"

if [ -d /etc/selinux ]; then
    CHECK=`grep ^SELINUX= /etc/selinux/config 2>/dev/null | sed 's/.*=//'`
    if [ -z "$CHECK" ]; then
        logger -p local6.notice -t installer "netifyos-base-security - setting SELinux to permissive"
        echo "SELINUX=permissive" >> /etc/selinux/config
    elif [ "$CHECK" != "permissive" ]; then
        logger -p local6.notice -t installer "netifyos-base-security - changing SElinux to permissive"
        sed -i -e 's/^SELINUX=.*/SELINUX=permissive/' /etc/selinux/config
    fi
fi

%files
%defattr(-,root,root)
/etc/security/limits.d/95-netifyos.conf

%changelog
* Tue May  1 2018 Netify <team@netify.ai> - 7.5.0-1
- Rebuild for NetifyOS 7.5.0 release

* Sat Mar 31 2018 Netify <team@netify.ai> - 7.4.0-1
- First build
